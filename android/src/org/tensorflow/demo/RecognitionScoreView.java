/* Copyright 2015 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

package org.tensorflow.demo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.AudioManager;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.media.MediaPlayer;

import org.tensorflow.demo.Classifier.Recognition;

import java.util.ArrayList;
import java.util.List;

public class RecognitionScoreView extends View implements ResultsView {
  private static final float TEXT_SIZE_DIP = 24;
  private List<Recognition> results;
  private final float textSizePx;
  private final Paint fgPaint;
  private final Paint bgPaint;
  private float averageConf;
  private List<Recognition> lastRecogs;
  private Recognition best;
  public static String bestTitle;
  private AudioManager auxControl;
  public static boolean isReading;
  public MediaPlayer mp;
  public RecognitionScoreView(final Context context, final AttributeSet set) {
    super(context, set);
    textSizePx =
            TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE_DIP, getResources().getDisplayMetrics());
    fgPaint = new Paint();
    fgPaint.setTextSize(textSizePx);
    bgPaint = new Paint();
    bgPaint.setColor(0xcc4285f4);
    averageConf = 0;
    isReading = true;
    lastRecogs = new ArrayList<Recognition>();
    mp = new MediaPlayer();
  }

  public void adjustVolume(final int keyCode){
    if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN){
      auxControl.adjustStreamVolume( AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER,0);
    }
    if (keyCode == KeyEvent.KEYCODE_VOLUME_UP){
      auxControl.adjustStreamVolume( AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE,0);
    }
  }

  @Override
  public void setResults(final List<Recognition> results) {
    this.results = results;
    postInvalidate();
  }

  @Override
  public void onDraw(final Canvas canvas) {
    final int x = 10;
    int y = (int) (fgPaint.getTextSize() * 1.5f);
    canvas.drawPaint(bgPaint);
    if (results != null) {
      if(isReading) weightedConf();
      float bestConf = results.get(0).getConfidence();
      String bestTitle = results.get(0).getTitle();
      canvas.drawText(bestTitle + ": " + bestConf, x, y, fgPaint);
      y += fgPaint.getTextSize() * 1.5f;
    }
  }

  public void weightedConf(){
    best = results.get(0);
    if(lastRecogs.isEmpty()){lastRecogs.add(best);}
    else if(lastRecogs.get(0).getTitle().equals(best.getTitle())
            && lastRecogs.size() < 4){lastRecogs.add(best);}
    else if (lastRecogs.size() == 4){
        float sumconf= 0;
        for(Recognition recog: lastRecogs){
            sumconf = sumconf+recog.getConfidence();
        }
        if(sumconf/lastRecogs.size() > 0.75){
            playSound(lastRecogs.get(0).getTitle());
        }
        lastRecogs.clear();lastRecogs.add(best);}
    else {lastRecogs.clear(); lastRecogs.add(best);}
  }
  public void playSound(String filename) {
      if(!mp.isPlaying()) {
          mp.reset();
            try {Context context = getContext();
                mp.setDataSource(context.getApplicationContext(), Uri.parse("android.resource://org.tensorflow.demo/raw/ses"+filename));}
            catch (Exception e) { }
            try {mp.prepare();mp.start();}
            catch (Exception e) { }
        }
    }
}
